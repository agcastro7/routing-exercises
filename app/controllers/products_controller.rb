class ProductsController < ApplicationController
  before_action :set_product, except: [:new, :create, :index, :clear_stock, :search]

  def index
    @products = params[:stock] ? Product.where(stock: params[:stock]) : Product.all
  end

  def show
  end

  def new
    if Category.count == 0
      redirect_to new_category_path
    else
      @product = Product.new
    end

  end

  def edit
  end

  def create
    @product = Product.new(product_params)

    if @product.save
      redirect_to @product, notice: 'Product was successfully created.'
    else
      render :new
    end
  end

  def update
    if @product.update(product_params)
      redirect_to @product, notice: 'Product was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @product.destroy
    redirect_to products_url, notice: 'Product was successfully destroyed.'
  end

  def clear_stock
    Product.update_all(stock: 0)
    redirect_to products_url, notice: 'Product stock was cleared.'
  end

  def increment_stock
    if @product.update(stock: @product.stock + 1)
      redirect_to category_product_path(@product.category, @product)
    else
      redirect_to category_product_path(@product.category, @product), error: 'Stock could not be updated'
    end
  end

  def decrement_stock
    if @product.stock == 0
      redirect_to category_product_path(@product.category, @product), error: 'Stock is already 0'
    elsif @product.update(stock: @product.stock - 1)
      redirect_to category_product_path(@product.category, @product)
    else
      redirect_to category_product_path(@product.category, @product), error: 'Stock could not be updated'
    end
  end

  def search
    query = params.require(:query)
    @products = Product.where('name like ?', "% #{query} %")
    render :index
  end

  private
    def set_product
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit(:name, :image, :description, :stock, :category_id)
    end
end
