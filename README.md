# Routing exercises

To download this project, execute in bash:

```bash
git clone git@bitbucket.org:agcastro7/routing-exercises.git
cd routing-exercises
bundle
rails db:migrate
rails s
```
